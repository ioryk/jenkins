# Project examples

## Requirements
git version 2+

## Description
Dockerfiles for images:
1. _Dockerjenkinsall_: Dockerfile to build jenkins:latest image with all basic plugins
1. _DockerJnlpslave_: Dockerfile to build jenkins-jnlp slave with maven
1. _Dockermes-procc_: Dockerfile to build message-processor app
1. _Dockergateway_: Dockerfile to build message-gateway app


## Container download list:
```
docker pull iorykster/devops:jenkins-with-allplugins
```
```
docker pull iorykster/devops:jnlp-slave
```
```
docker pull iorykster/devops:message-processor
```
```
docker pull iorykster/devops:message-gateway
```
